# Web Curriculum Vitae

Si scriva con il linguaggio html un curriculum vitae (standard a piacere) utilizzando una o piu' immagini (consone) e impiegando i file css per la formattazione del testo.

Per la creazione della repository il file html dovra' risiedere nella directory radice.
- Le immagini dovranno trovarsi in una apposita sotto directory
- Eventuali file css dovranno risiedere in una cartella a parte

Tutto il progetto dovra' essere caricato nella piattaforma mediante l'utilizzo di git.

 
